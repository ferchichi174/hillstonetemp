<?php
/**
 * @package pro
 */
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="progression-single-width-container">
		<div id="progression-blog-single-content">
			
			<?php if(has_post_thumbnail()): ?>
				<div class="progression-studios-feaured-image">
						<?php the_post_thumbnail('progression-studios-blog-index'); ?>
				</div><!-- close .progression-studios-feaured-image -->
			<?php else: ?>
			
			<?php if( get_post_meta($post->ID, 'progression_studios_video_post', true)  ): ?>
				<div class="progression-studios-feaured-image video-progression-studios-format">
					<?php echo apply_filters('progression_studios_video_content_filter', get_post_meta($post->ID, 'progression_studios_video_post', true)); ?>
				</div>
			<?php endif; ?><!-- close video -->
			<?php endif; ?><!-- close featured thumbnail -->
			
			<div class="progression-blog-content">
			
			
				<?php if ( get_theme_mod( 'progression_studios_blog_post_meta_date_display', 'true') == 'true' && 'post' == get_post_type() ) : ?>	
					<div class="blog-meta-date-list"><a href="<?php echo get_month_link(get_the_time('Y'), get_the_time('m')); ?>"><?php the_time(get_option('date_format')); ?></a></div>
				<?php endif; ?>
	
				<h2 class="progression-blog-title"><?php the_title(); ?></h2>
		
		
				<?php if ( 'post' == get_post_type()  ) : ?>
					<ul class="progression-post-meta">
						<?php if (get_theme_mod( 'progression_studios_blog_post_meta_author_display', 'true') == 'true') : ?><li class="blog-meta-author-display"><?php echo esc_html__( 'By', 'stone-hill-progression' ); ?> <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php the_author(); ?></a></li><?php endif; ?>
						
						<?php if (get_theme_mod( 'progression_studios_blog_post_index_meta_category_display', 'true') == 'true') : ?>
							<li class="blog-meta-category-list"><?php echo esc_html__( 'in', 'stone-hill-progression' ); ?> <?php the_category(', '); ?></li>
						<?php endif; ?>

						<?php if (get_theme_mod( 'progression_studios_blog_post_meta_comment_display', 'true') == 'true') : ?><?php if ( comments_open() ) : ?><li class="blog-meta-comments"><?php echo esc_html__( 'with', 'stone-hill-progression' ); ?> <?php comments_popup_link( '' . wp_kses( __( '0 Comments', 'stone-hill-progression' ), true ) . '', wp_kses( __( '1 Comment', 'stone-hill-progression' ), true), wp_kses( __( '% Comments', 'stone-hill-progression' ), true ) ); ?></li><?php endif; ?><?php endif; ?>
					</ul>
					<div class="clearfix-pro"></div>
				<?php endif; ?>
			
			
				<?php the_content(); ?>
			
				<?php wp_link_pages( array(
					'before' => '<div class="progression-page-nav">' . esc_html__( 'Pages:', 'stone-hill-progression' ),
					'after'  => '</div><div class="clearfix-pro"></div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
					) );
				?>
			
				<div class="clearfix-pro"></div>
			
				<?php the_tags(  '<div class="tags-progression-studios">', ' ', '</div>' ); ?>
				<div class="clearfix-pro"></div>
			
			</div><!-- close .progression-blog-content -->
				
				<?php if ( comments_open() || get_comments_number() ) : comments_template(); endif; ?>
					<div class="clearfix-pro"></div>
					
					
		</div><!-- close .progression-blog-single-content -->
	</div><!-- close .progression-single-width-container -->
</div><!-- #post-## -->